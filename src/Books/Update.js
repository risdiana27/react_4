import React, {useState, useEffect} from 'react';
import axios from 'axios';
import ReactDOM from "react-dom";
import { useForm } from "react-hook-form";

import "./index.css";

export default function Update(props) {

	const url = 'http://127.0.0.1:3000/books/'

	const [Book, setBook] = useState({data: []})
	const [data, setData] = useState({
		title: '',
		author: '',
		publisher_date: '',
		pages: '',
		language: '',
		published_id: ''
	})


	const { register, errors } = useForm({
		mode: "onChange"
	});

	// const onSubmit = data => {
	// 	alert(JSON.stringify(data));
	// };

	useEffect(
	()=>{
		const id = props.match.params.id
		axios.get(url+id)
		.then(res=>{
			console.log(res.data)
			const mydata={...Book}
			setBook(mydata)
			setData(res.data)
		}).catch(err=>console.error(err))
	},[]
	)

	function submit(e){
		e.preventDefault()
		const id = props.match.params.id
		axios.put(url+id, data)
		.then(res=>{
			//console.log(res.data)
			props.history.push('/books')
		}).catch(err=>console.error(err))
	}

	function handle(e){
		const newdata={...data}
		newdata[e.target.id]=e.target.value
		setData(newdata)
	}

	return (
  	<div className="container">
	    <form onSubmit={(e)=>submit(e)}>
		    <div className="form-group">
		      <label htmlFor="title">Title</label>
		      <input
		        id="title"
		      	type="text"
		      	placeholder="Title"
		      	name="title"
		      	value={data.title}
		      	onChange={(e)=>handle(e)}
		      	className="form-control"
		      	ref={register({
		      		required: "This is required",
		      		minLength: {
		      			value: 3,
		      			message: "Min length is 3"
		      		}
		      	})}
		      />
		      {errors.title && <p>{errors.title.message}</p>}
		    </div>
	        <div className="form-group">
		      <label htmlFor="author">Author</label>
		      <input
		        id="author"
		      	type="text"
		      	placeholder="Author"
		      	name="author"
		      	value={data.author}
		      	onChange={(e)=>handle(e)}
		      	className="form-control"
		      	ref={register({
		      		required: "This is required",
		      		minLength: {
		      			value: 3,
		      			message: "Min length is 3"
		      		}
		      	})}
		      />
		      {errors.author && <p>{errors.author.message}</p>}
	        </div>
	        <div className="form-group">
		      <label htmlFor="publisher_date">Publisher Date</label>
		      <input
		        id="publisher_date"
		      	type="date"
		      	placeholder="Publisher Date"
		      	name="publisher_date"
		      	value={data.publisher_date}
		      	onChange={(e)=>handle(e)}
		      	className="form-control"
		      	ref={register({
		      		required: "This is required"
		      	})}
		      />
		      {errors.publisher_date && <p>{errors.publisher_date.message}</p>}
	        </div>
	        <div className="form-group">
		      <label htmlFor="pages">Pages</label>
		      <input
		        id="pages"
		      	type="number"
		      	placeholder="Pages"
		      	name="pages"
		      	value={data.pages}
		      	onChange={(e)=>handle(e)}
		      	className="form-control"
		      	ref={register({
		      		required: "This is required"
		      	})}
		      />
		      {errors.pages && <p>{errors.pages.message}</p>}
	        </div>
	        <div className="form-group">
	      	  <label htmlFor="language">Language</label>
		      <input
		        id="language"
		      	type="text"
		      	placeholder="Language"
		      	name="language"
		      	value={data.language}
		      	onChange={(e)=>handle(e)}
		      	className="form-control"
		      	ref={register({
		      		required: "This is required",
		      		minLength: {
		      			value: 3,
		      			message: "Min length is 3"
		      		}
		      	})}
		      />
		      {errors.language && <p>{errors.language.message}</p>}
	        </div>
	        <div className="form-group">
		      <label htmlFor="published_id">Published ID</label>
		      <input
		        id="published_id"
		      	type="text"
		      	placeholder="Published ID"
		      	name="published_id"
		      	value={data.published_id}
		      	onChange={(e)=>handle(e)}
		      	className="form-control"
		      	ref={register({
		      		required: "This is required",
		      		minLength: {
		      			value: 3,
		      			message: "Min length is 3"
		      		}
		      	})}
		      />
		      {errors.published_id && <p>{errors.published_id.message}</p>}
	        </div>
	      <button className="btn btn-success btn-sm">Submit</button>
	    </form>
    </div>
  );
}


// import React, {useState, useEffect} from 'react';
// import axios from 'axios';

// export default function Update (props) {

// 	const url = 'http://127.0.0.1:3000/books/'

// 	const[Book, setBook] = useState({data: []})
// 	const [data, setData] = useState({
// 		title: '',
// 		author: '',
// 		publisher_date: '',
// 		pages: '',
// 		language: '',
// 		published_id: ''
// 	})

// 	useEffect(
// 		()=>{
// 			const id = props.match.params.id
// 			axios.get(url+id)
// 			.then(res=>{
// 				console.log(res.data)
// 				const mydata={...Book}
// 				setBook(mydata)
// 				setData(res.data)
// 			}).catch(err=>console.error(err))
// 		},[]
// 	)

// 	function submit(e){
// 		e.preventDefault()
// 		const id = props.match.params.id
// 		axios.put(url+id, data)
// 		.then(res=>{
// 			//console.log(res.data)
// 			props.history.push('/books')
// 		}).catch(err=>console.error(err))
// 	}

// 	function handle(e){
// 		const newdata={...data}
// 		newdata[e.target.id]=e.target.value
// 		setData(newdata)
// 	}

// 	return(
// 		<div className="container">
// 			<form onSubmit={(e)=>submit(e)}>
// 				<div className="form-group">
// 		          <label htmlFor="title">Title</label>
// 		          <input onChange={(e)=>handle(e)} value={data.title} type="text" name="title" id="title" className="form-control" placeholder="Title"/>
// 		        </div>
// 		        <div className="form-group">
// 		          <label htmlFor="author">Author</label>
// 		          <input onChange={(e)=>handle(e)} value={data.author} type="text" name="author" id="author" className="form-control" placeholder="Author"/>
// 		        </div>
// 		        <div className="form-group">
// 		          <label htmlFor="publisher_date">Pubelisher Date</label>
// 		          <input onChange={(e)=>handle(e)} value={data.publisher_date} type="date" name="publisher_date" id="publisher_date" className="form-control" placeholder="Pubelished Date"/>
// 		        </div>
// 		        <div className="form-group">
// 		          <label htmlFor="pages">Pages</label>
// 		          <input onChange={(e)=>handle(e)} value={data.pages} type="number" name="pages" id="pages" className="form-control" placeholder="Pages"/>
// 		        </div>
// 		        <div className="form-group">
// 		          <label htmlFor="language">Language</label>
// 		          <input onChange={(e)=>handle(e)} value={data.language} type="text" name="language" id="language" className="form-control" placeholder="Language"/>
// 		        </div>
// 		        <div className="form-group">
// 		          <label htmlFor="published_id">Published ID</label>
// 		          <input onChange={(e)=>handle(e)} value={data.published_id} type="text" name="published_id" id="published_id" className="form-control" placeholder="Publisher Id"/>
// 		        </div>
// 		        <button className="btn btn-success btn-sm">Submit</button>
// 			</form>
// 		</div> 
// 	)
// }