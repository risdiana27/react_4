import React, {useState, useEffect} from 'react';
import axios from 'axios';

export default function Book(props){

	const url = 'http://127.0.0.1:3000/books/'

	const[Book, setBook] = useState({data: []})
	// const [data, setData] = useState({
	// 	title: '',
	// 	author: '',
	// 	published_date: '',
	// 	pages: '',
	// 	language: '',
	// 	publisher_id: ''
	// })

	useEffect(
		()=>{
			axios.get(url)
			.then(res=>{
				console.log(res.data)
				setBook(res.data)
			}).catch(err=>console.error(err))
		},[]
	)

	// useEffect(() => {
 //     const fetchData = async () => {
 //       const result = await axios(url);
 //       setData(result.data);
 //     };
 //     fetchData();
 //   }, []);

 	function remove(id){
 		//console.log(id)
 		axios.delete(url+id)
 		.then(res=>{
 			console.log(res.data)
 			props.history.push('/books')
 		}).catch(err=>console.error(err))
 	}

	// function submit(e){
	// 	e.preventDefault()
	// 	axios.post(url, data)
	// 	.then(res=>{
	// 		console.log(res.data)
	// 		const mydata={...Book}
	// 		setBook(mydata)
	// 	}).catch(err=>console.error(err))
	// }

	// function handle(e){
	// 	const newdata={...data}
	// 	newdata[e.target.id]=e.target.value
	// 	setData(newdata)
	// }

	function update(id){
		console.log(id)
		props.history.push('/update/'+id)
	}

	function add(){
		props.history.push('/addbook')
	}

	return(
		<div className="container">
			<button onClick={()=>add()} className="btn btn-success btn-sm float-right">+ Add Book</button>
			<table className="table table-hover">
				<thead>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Published</th>
						<th>Published</th>
						<th>Published</th>
						<th>Published</th>
						<th>Action</th>
					</tr>
	       		</thead>
	       		<tbody>{Book.data.map(book => (
			         <tr key = {book.id}>
			           <td>{book.title}</td>
			           <td>{book.author}</td>
			           <td>{book.publisher_date}</td>
			           <td>{book.pages}</td>
			           <td>{book.language}</td>
			           <td>{book.published_id}</td>
			           <td>
			             <button onClick={()=>update(book.id)} className="btn btn-success">Update</button>|
			             <button onClick={()=>remove(book.id)} className="btn btn-danger">Delete</button>
			           </td>
			         </tr>
			       ))}
			     </tbody>
			</table>
		</div>
	)
}