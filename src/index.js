import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Book from './Books/Book.js';
import Update from './Books/Update.js';
import AddBook from './Books/AddBook';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigasi from './Navbar/Navbar';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

const routing = (
  <Router>
    <Navigasi />
  <Switch>
    <Route exact path="/" component={App} />
    <Route path="/books" component={Book} />
  	<Route path="/update/:id" component={Update} />
  	<Route path="/addbook" component={AddBook} />
  </Switch>
  </Router>
  
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
